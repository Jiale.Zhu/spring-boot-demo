package com.zjl.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zjl.model.AddScoreReq;
import com.zjl.model.BaseRsp;
import com.zjl.model.DeleteScoreReq;
import com.zjl.model.QueryListReq;
import com.zjl.model.QueryListRsp;
import com.zjl.model.UpdateScoreReq;
import com.zjl.service.StudentService;

@RestController
@RequestMapping("/student")
public class StudentController {
	private static final Logger LOG = LoggerFactory.getLogger(StudentController.class);
	
	@Autowired
	private StudentService studentService;
	
	@RequestMapping("/hello")
	public String index() {
		return "Hello World";
	}

	@RequestMapping(value = "/queryList", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public QueryListRsp queryList(@RequestBody @Validated QueryListReq req) {
		LOG.info("req = {}", req);
		QueryListRsp rsp = studentService.queryList(req.getXh());
		LOG.info("rsp = {}", rsp);
		return rsp;
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BaseRsp add(@RequestBody @Validated AddScoreReq req) {
		LOG.info("req = {}", req);
		BaseRsp rsp = studentService.add(req);
		LOG.info("rsp = {}", rsp);
		return rsp;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BaseRsp delete(@RequestBody @Validated DeleteScoreReq req) {
		LOG.info("req = {}", req);
		BaseRsp rsp = studentService.delete(req);
		LOG.info("rsp = {}", rsp);
		return rsp;
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BaseRsp update(@RequestBody @Validated UpdateScoreReq req) {
		LOG.info("req = {}", req);
		BaseRsp rsp = studentService.update(req);
		LOG.info("rsp = {}", rsp);
		return rsp;
	}
	
}
