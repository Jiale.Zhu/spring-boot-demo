package com.zjl.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zjl.dao.Course;
import com.zjl.dao.StudentDao;
import com.zjl.model.AddScoreReq;
import com.zjl.model.BaseRsp;
import com.zjl.model.DeleteScoreReq;
import com.zjl.model.QueryListRsp;
import com.zjl.model.UpdateScoreReq;

@Component
public class StudentService {
	private static final Logger LOG = LoggerFactory.getLogger(StudentService.class);
	
	@Autowired
	private StudentDao studentDao;
	
	public QueryListRsp queryList(String xh) {
		QueryListRsp rsp = new QueryListRsp();
		List<Course> records = studentDao.queryList(xh);
		
		rsp.setRecords(records);
		return rsp;
	}

	public BaseRsp add(AddScoreReq req) {
		BaseRsp rsp = new BaseRsp();
		
		studentDao.add(req.getScoreInfo());
		
		return rsp;
	}

	public BaseRsp delete(DeleteScoreReq req) {
		BaseRsp rsp = new BaseRsp();
		studentDao.delete(req.getScoreInfo());
		return rsp;
	}

	public BaseRsp update(UpdateScoreReq req) {
		BaseRsp rsp = new BaseRsp();
		studentDao.update(req.getScoreInfo());
		return rsp;
	}


}
