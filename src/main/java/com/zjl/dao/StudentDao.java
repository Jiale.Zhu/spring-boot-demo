package com.zjl.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface StudentDao {
    @Select("SELECT * FROM bjkc a INNER JOIN xj b ON a.bjh = b.bh AND b.xh = #{xh}")
	List<Course> queryList(@Param("xh") String xh);

    @Insert("insert into kccj (xh, kcbh, kcmc, cj,xnxq) values (#{xh}, #{kcbh}, #{kcmc}, #{cj}, #{xnxq})")    
	void add(ScoreInfo scoreInfo);

    @Delete("delete from kccj where xh=#{xh} and kcbh=#{kcbh} and xnxq=#{xnxq}")
    void delete(ScoreInfo scoreInfo);

    @Update("update kccj set cj=#{cj} where xh=#{xh} and kcbh=#{kcbh} and xnxq=#{xnxq}")
    void update(ScoreInfo scoreInfo);
}
