package com.zjl.dao;

public class Course {
	private String XNXQ;
	private String KCH;
	private String KCMC;
	private String ZCSM;
	private String SKXQ;
	private String SKJC;
	private String CXJC;
	private String XQM;
	private String JXLH;
	private String JXLM;
	private String JASH;
	private String JASM;
	private String BJH;
	private String BM;
	public String getXNXQ() {
		return XNXQ;
	}
	public void setXNXQ(String xNXQ) {
		XNXQ = xNXQ;
	}
	public String getKCH() {
		return KCH;
	}
	public void setKCH(String kCH) {
		KCH = kCH;
	}
	public String getKCMC() {
		return KCMC;
	}
	public void setKCMC(String kCMC) {
		KCMC = kCMC;
	}
	public String getZCSM() {
		return ZCSM;
	}
	public void setZCSM(String zCSM) {
		ZCSM = zCSM;
	}
	public String getSKXQ() {
		return SKXQ;
	}
	public void setSKXQ(String sKXQ) {
		SKXQ = sKXQ;
	}
	public String getSKJC() {
		return SKJC;
	}
	public void setSKJC(String sKJC) {
		SKJC = sKJC;
	}
	public String getCXJC() {
		return CXJC;
	}
	public void setCXJC(String cXJC) {
		CXJC = cXJC;
	}
	public String getXQM() {
		return XQM;
	}
	public void setXQM(String xQM) {
		XQM = xQM;
	}
	public String getJXLH() {
		return JXLH;
	}
	public void setJXLH(String jXLH) {
		JXLH = jXLH;
	}
	public String getJXLM() {
		return JXLM;
	}
	public void setJXLM(String jXLM) {
		JXLM = jXLM;
	}
	public String getJASH() {
		return JASH;
	}
	public void setJASH(String jASH) {
		JASH = jASH;
	}
	public String getJASM() {
		return JASM;
	}
	public void setJASM(String jASM) {
		JASM = jASM;
	}
	public String getBJH() {
		return BJH;
	}
	public void setBJH(String bJH) {
		BJH = bJH;
	}
	public String getBM() {
		return BM;
	}
	public void setBM(String bM) {
		BM = bM;
	}
	
	
}
