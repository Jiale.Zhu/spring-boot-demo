package com.zjl.dao;

public class ScoreInfo {
	private String xh;
	private String kcbh;
	private String kcmc;
	private String cj;
	private String xnxq;
	public String getXh() {
		return xh;
	}
	public void setXh(String xh) {
		this.xh = xh;
	}
	public String getKcbh() {
		return kcbh;
	}
	public void setKcbh(String kcbh) {
		this.kcbh = kcbh;
	}
	public String getKcmc() {
		return kcmc;
	}
	public void setKcmc(String kcmc) {
		this.kcmc = kcmc;
	}
	public String getCj() {
		return cj;
	}
	public void setCj(String cj) {
		this.cj = cj;
	}
	public String getXnxq() {
		return xnxq;
	}
	public void setXnxq(String xnxq) {
		this.xnxq = xnxq;
	}
	
	
	
}
