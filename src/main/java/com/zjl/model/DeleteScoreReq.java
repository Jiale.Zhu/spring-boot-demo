package com.zjl.model;

import com.zjl.dao.ScoreInfo;

public class DeleteScoreReq {
	
	private ScoreInfo scoreInfo;

	public ScoreInfo getScoreInfo() {
		return scoreInfo;
	}

	public void setScoreInfo(ScoreInfo scoreInfo) {
		this.scoreInfo = scoreInfo;
	}

	
}
