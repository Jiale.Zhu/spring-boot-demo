package com.zjl.model;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class QueryListReq {
	
	@NotNull
	@Valid
	private String xh;

	public String getXh() {
		return xh;
	}

	public void setXh(String xh) {
		this.xh = xh;
	}
	
	

}
