package com.zjl.model;

import javax.validation.Valid;

import com.zjl.dao.ScoreInfo;

public class UpdateScoreReq {
	@Valid
	private ScoreInfo scoreInfo;

	public ScoreInfo getScoreInfo() {
		return scoreInfo;
	}

	public void setScoreInfo(ScoreInfo scoreInfo) {
		this.scoreInfo = scoreInfo;
	}

	
}
