package com.zjl.model;

import java.util.List;

import com.zjl.dao.Course;

public class QueryListRsp extends BaseRsp {
	
	private List<Course> records;

	public List<Course> getRecords() {
		return records;
	}

	public void setRecords(List<Course> records) {
		this.records = records;
	}
	
	
}
