package com.zjl.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zjl.model.BaseRsp;

@ControllerAdvice
public class MyControllerAdvice {
	private static final Logger LOG = LoggerFactory.getLogger(MyControllerAdvice.class);

    /**
     * 全局异常捕捉处理
     * @param ex
     * @return
     */
    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public BaseRsp errorHandler(Exception ex) {
    	LOG.error("ExceptionHandler", ex);
    	BaseRsp baseRsp = new BaseRsp();
    	baseRsp.setCode(-1);
    	baseRsp.setDesc(ex.getMessage());
        return baseRsp;
    }

}