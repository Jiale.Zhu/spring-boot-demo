# spring-boot-demo
A demo about SpringBoot.

# Prerequisite
- Jdk 1.8
- Maven 3.5.2+

# Build
mvn clean install
